
/* globals CONFIG */

export const isProduction = process.env.NODE_ENV === 'production';

export const s3 = CONFIG.s3;
export const web = getEnvConfig(CONFIG.web);
export const products = getEnvConfig(CONFIG.products);
export const auth = getEnvConfig(CONFIG.auth);

function getEnvConfig(service) {
    const { dev, production } = service;
    if (isProduction) {
        return production;
    }
    return dev;
}
