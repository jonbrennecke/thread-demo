
import React from 'react';
import { render } from 'react-dom';
import Promise from 'bluebird';

import AppProvider from './AppProvider';
import RouteProvider from './RouteProvider';

import '../scss/main.scss';
import 'normalize.css';

Promise.config({
    warnings: {
        wForgottenReturn: false
    }
});

const RootComponent = () => {
    return (
        <AppProvider>
            <RouteProvider/>
        </AppProvider>
    );
};

render(<RootComponent/>, document.getElementById('react-main'));
