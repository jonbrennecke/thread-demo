
import React from 'react';
import {
    Router,
    Route,
    IndexRedirect,
    browserHistory
} from 'react-router';

import CommentThreadPage from './pages/CommentThreadPage';
import App from './App';

export default function RouteProvider() {
    return (
        <Router history={browserHistory}>
            <Route path="/" component={App}>
                <IndexRedirect to="comments"/>
                <Route path="comments" component={CommentThreadPage}/>
            </Route>

        </Router>
    );
}
