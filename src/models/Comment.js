
import { Model } from 'caldera-model';

export default class Comment extends Model {

    constructor(data) {
        super(data);
        const CommentCollection = require('./CommentCollection').default;
        this.childComments = new CommentCollection(this.get('childComments'));
    }

    getId() {
        return this.get('id');
    }

    getParentCommentId() {
        return this.get('parentCommentId');
    }

    getBody() {
        return this.get('body');
    }

    getUsername() {
        return this.get('username');
    }

    getChildComments() {
        return this.childComments;
    }

    setChildComments(childComments) {
        return this.set('childComments', childComments);
    }
}
