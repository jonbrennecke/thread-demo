
import { Model } from 'caldera-model';

export default class Post extends Model {

    getId() {
        return this.get('id');
    }

    getBody() {
        return this.get('body');
    }

    getUsername() {
        return this.get('username');
    }

    getTitle() {
        return this.get('title');
    }
}
