
import { Collection } from 'caldera-model';
import Comment from './Comment';

export default class CommentCollection extends Collection {

    static Model = Comment

    findAllByParentCommentId(parentId) {
        return this.filter(comment => comment.getParentCommentId() === parentId);
    }

    findAllTopLevelComments() {
        return this.filter(comment => !comment.getParentCommentId());
    }
}
