
import React from 'react';
import merge from 'lodash/merge';
import { Provider as ReduxProvider } from 'react-redux';
import { ThemeProvider } from 'react-css-themr';

import { theme as componentsTheme } from './components';
import { theme as pagesTheme } from './pages';
import store from './store';


export default function AppProvider({ children }) {
    return (
        <ReduxProvider store={store}>
            <ThemeProvider theme={merge(pagesTheme, componentsTheme)}>
                {children}
            </ThemeProvider>
        </ReduxProvider>
    );
}
