
import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { themr } from 'react-css-themr';
import map from 'lodash/map';
import classnames from 'classnames';

import CommentCollection from '../models/CommentCollection';
import CommentComposer from '../components/CommentComposer';
import CommentView from '../components/CommentView';
import PostView from '../components/PostView';
import { COMMENTS } from '../redux';
import Post from '../models/Post';

class CommentThreadPage extends Component {

    static propTypes = {
        className: PropTypes.string,
        theme: PropTypes.object.isRequired,
        commentTree: PropTypes.instanceOf(CommentCollection).isRequired
    }

    render() {
        const { className, theme, post } = this.props;
        return (
            <div className={classnames(theme.container, className)}>
                <PostView post={post}/>
                <CommentComposer/>
                {this.renderCommentTree()}
            </div>
        );
    }

    renderCommentTree() {
        const commentTree = this.props.commentTree;
        return map(commentTree.toArray(), (comment, index) => (
            <CommentView comment={comment}/>
        ));
    }
}

export default themr('CommentThreadPage')(
    connect(
        state => ({
            commentTree: state[COMMENTS].getCommentTree(),
            post: new Post({
                title: 'Hire me!',
                username: '@jonbrennecke',
                body: `Hi there! My name's Jon and I'm a React dev from Portland, Oregon.`
            })
        }),
        dispatch => ({})
    )(CommentThreadPage)
);
