
import { COMMENTS, commentsReducer } from './';

export default {
    [COMMENTS]: commentsReducer
};
