
import Promise from 'bluebird';
import uuid from 'uuid';

import {
    WILL_CREATE_COMMENT,
    CREATE_COMMENT_SUCCESS,
    CREATE_COMMENT_FAILURE } from './constants';

export function createComment(comment) {
    return (dispatch) => {
        dispatch({ type: WILL_CREATE_COMMENT });
        return Promise.resolve()
            .then(() => dispatch({
                type: CREATE_COMMENT_SUCCESS,
                comment: {
                    ...comment,
                    id: uuid.v4()
                }
            }))
            .catch(error => dispatch({
                type: CREATE_COMMENT_FAILURE
            }));
    };
}
