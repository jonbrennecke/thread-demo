
import { handleActions } from 'redux-actions';

import CommentState from './CommentState';
import { CREATE_COMMENT_SUCCESS } from './constants';

const initialState = new CommentState();

export default handleActions({
    [CREATE_COMMENT_SUCCESS]: (state, { comment }) => {
        return state.addOrReplaceComment(comment);
    }
}, initialState);
