
export { COMMENTS } from './constants';

export reducer from './reducer';

export { createComment } from './actions';
