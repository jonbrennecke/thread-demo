
import { Model } from 'caldera-model';
import CommentCollection from '../../models/CommentCollection';

export default class CommentState extends Model {

    constructor(data) {
        super(data);
        this.comments = new CommentCollection(this.get('comments'));
    }

    getComments() {
        return this.comments;
    }

    setComments(comments) {
        return this.set('comments', comments);
    }

    addOrReplaceComment(comment) {
        const id = comment.id || comment.getId();
        const comments = this.getComments()
                             .addOrReplaceById(id, comment);
        return this.setComments(comments);
    }

    getCommentTree() {
        const comments = this.getComments();
        const topLevelComments = comments.findAllTopLevelComments();
        return groupComments(topLevelComments, comments);
    }
}

// Note: Prevent too much recursion by limiting comment nesting depth to 50
function groupComments(comments, allComments, recursionDepth = 0, recursionLimit = 50) {
    if (recursionDepth >= recursionLimit) {
        return comments;
    }
    return comments.map((comment) => {
        const childComments = allComments.findAllByParentCommentId(comment.getId());
        const childCommentsWithChildren = groupComments(childComments, allComments, recursionDepth + 1, recursionLimit);
        return comment.setChildComments(childCommentsWithChildren);
    });
}
