
import { createStore, combineReducers, applyMiddleware } from 'redux';
import * as storage from 'redux-storage';
import immutableMerger from 'redux-storage-merger-immutablejs';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';

import { reducers } from './redux';

const loggerMiddleware = createLogger();
const rootReducer = storage.reducer(
    combineReducers(reducers),
    immutableMerger
);

const middleware = applyMiddleware(
    thunkMiddleware,
    loggerMiddleware
);

export default createStore(rootReducer, middleware);
