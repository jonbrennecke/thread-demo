
import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import { themr } from 'react-css-themr';
import Post from '../models/Post';

class PostView extends Component {

    static propTypes = {
        className: PropTypes.string,
        theme: PropTypes.object.isRequired,
        post: PropTypes.instanceOf(Post).isRequired
    }

    render() {
        const { className, theme, post } = this.props;
        return (
            <div className={classnames(className, theme.container)}>
                <h2>{post.getTitle()}</h2>
                <h3>{post.getUsername()}</h3>
                <p>{post.getBody()}</p>
            </div>
        );
    }
}

export default themr('PostView')(PostView);
