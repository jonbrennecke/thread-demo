
export CommentComposer from './CommentComposer';
export CommentView from './CommentView';

export const theme = {
    CommentComposer: require('./CommentComposer.styles.scss'),
    CommentView: require('./CommentView.styles.scss'),
    PostView: require('./PostView.styles.scss')
};
