
import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import { themr } from 'react-css-themr';
import map from 'lodash/map';

import Comment from '../models/Comment';
import CommentComposer from './CommentComposer';

class CommentView extends Component {

    static propTypes = {
        className: PropTypes.string,
        theme: PropTypes.object.isRequired,
        comment: PropTypes.instanceOf(Comment).isRequired,
        depth: PropTypes.number
    }

    static defaultProps = {
        depth: 0
    }

    state = {
        showReplyComposer: false
    }

    toggleReplyComposer() {
        this.setState({
            showReplyComposer: !this.state.showReplyComposer
        });
    }

    render() {
        const { className, theme } = this.props;
        return (
            <div className={classnames(className, theme.container)}>
                {this.renderCommentBody()}
                <div className={theme.nested}>
                    {this.renderReplyComposer()}
                    {this.renderChildComments()}
                </div>
            </div>
        );
    }

    renderCommentBody() {
        const { theme, comment, depth } = this.props;
        const showReply = depth < 4;
        return (
            <div className={theme.bodyContainer}>
                <div className={theme.body}>
                    <h3>{comment.getUsername()}</h3>
                    <p>{comment.getBody()}</p>
                </div>
                {showReply && <button onClick={this.toggleReplyComposer.bind(this)}>
                        Reply
                    </button>}
            </div>
        );
    }

    renderReplyComposer() {
        if (!this.state.showReplyComposer) {
            return;
        }
        return (
            <CommentComposer isReply={true}
                             parentCommentId={this.props.comment.getId()}
                             onSave={this.toggleReplyComposer.bind(this)}/>
        );
    }

    renderChildComments() {
        const { comment, theme, depth } = this.props;
        const childComments = comment.getChildComments();
        return map(childComments.toArray(), (cmt, index) => (
            <CommentView key={index}
                         comment={cmt}
                         theme={theme}
                         depth={depth + 1}/>
        ));
    }
}

export default themr('CommentView')(CommentView);
