
import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { themr } from 'react-css-themr';
import noop from 'lodash/noop';
import { createComment } from '../redux';

class CommentComposer extends Component {

    static propTypes = {
        className: PropTypes.string,
        theme: PropTypes.object.isRequired,
        createComment: PropTypes.func.isRequired,
        parentCommentId: PropTypes.string,
        onSave: PropTypes.func,
        isReply: PropTypes.bool
    }

    static defaultProps = {
        onSave: noop,
        isReply: false
    }

    state = {
        username: '',
        body: ''
    }

    saveComment() {
        const { username, body } = this.state;
        const { parentCommentId } = this.props;
        if (!username || !body) {
            return;
        }
        Promise.resolve()
            .then(() => this.props.createComment({
                username,
                body,
                parentCommentId
            }))
            .then(() => {
                this.setState({
                    username: '',
                    body: ''
                });
                this.props.onSave();
            });
    }

    render() {
        const { className, theme, isReply } = this.props;
        return (
            <div className={classnames(className, theme.container)}>
                <input value={this.state.username}
                       placeholder="Username"
                       onChange={e => this.setState({ username: e.target.value })}/>
                <textarea value={this.state.body}
                          placeholder="Leave your reply here..."
                          onChange={e => this.setState({ body: e.target.value })}/>
                <button onClick={this.saveComment.bind(this)}>
                    {isReply ? 'Add Reply' : 'Add Comment'}
                </button>
            </div>
        );
    }
}

export default themr('CommentComposer')(
    connect(
        (state) => ({}),
        (dispatch) => ({
            createComment: comment => dispatch(createComment(comment))
        })
    )(CommentComposer)
);
